from sqlalchemy import create_engine, text
# Press Shift+F10 to execute it or replace it with your code.

engine = create_engine("sqlite+pysqlite:///:memory:", echo=True, future=True)


def query():
    # conn
    with engine.connect() as conn:
        result = conn.execute(text("select 'hello world'"))
        # list<str>
        rst_all = result.all()
        print(rst_all)
        print(type(rst_all))


def main():
    query()


if __name__ == '__main__':
    main()

